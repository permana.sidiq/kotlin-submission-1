package com.nbs.footballteam

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.RecyclerView
import com.nbs.footballteam.R.array.club_description
import com.nbs.footballteam.R.array.club_name
import com.nbs.footballteam.adapter.FootballAdapter
import com.nbs.footballteam.model.Item
import com.nbs.footballteam.ui.MainActivityUI
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.setContentView
import org.jetbrains.anko.toast

class MainActivity : AppCompatActivity(), FootballAdapter.OnFootballTeamClickListener {

    val images = arrayOf("https://upload.wikimedia.org/wikipedia/commons/thumb/1/15/Juventus_FC_2017_logo.svg/110px-Juventus_FC_2017_logo.svg.png",
            "https://upload.wikimedia.org/wikipedia/en/thumb/4/47/FC_Barcelona_%28crest%29.svg/220px-FC_Barcelona_%28crest%29.svg.png",
            "https://upload.wikimedia.org/wikipedia/en/thumb/5/56/Real_Madrid_CF.svg/165px-Real_Madrid_CF.svg.png",
            "https://upload.wikimedia.org/wikipedia/en/thumb/1/1b/FC_Bayern_M%C3%BCnchen_logo_%282017%29.svg/220px-FC_Bayern_M%C3%BCnchen_logo_%282017%29.svg.png",
            "https://upload.wikimedia.org/wikipedia/en/thumb/7/7a/Manchester_United_FC_crest.svg/220px-Manchester_United_FC_crest.svg.png")

    private var items: MutableList<Item> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initData()

        val adapter = FootballAdapter(items)

        adapter.onFootballTeamClickListener = this

        MainActivityUI(adapter).setContentView(this)

        apply {
            supportActionBar?.title = "Football Team"
        }

    }

    private fun initData() {
        val names = resources.getStringArray(club_name)

        val descriptions = resources.getStringArray(club_description)

        for (i in names.indices){
            items.add(i, Item(names[i], images[i], descriptions[i]))
        }
    }

    override fun onFootballTeamClicked(item: Item) {
        startActivity(intentFor<DetailFootballTeamActivity>(DetailFootballTeamActivity.keyItem to item))
    }
}
