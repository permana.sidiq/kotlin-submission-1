package com.nbs.footballteam.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.nbs.footballteam.R
import com.nbs.footballteam.model.Item
import com.nbs.footballteam.ui.FootballItemUI
import kotlinx.android.synthetic.main.item_football_club.view.*
import org.jetbrains.anko.AnkoContext

/**
 * Created by sidiqpermana on 3/17/18.
 */
class FootballAdapter(val list: List<Item>): RecyclerView.Adapter<FootballAdapter.FootballViewholder>() {

    var onFootballTeamClickListener: OnFootballTeamClickListener? = null

    override fun onBindViewHolder(holder: FootballViewholder, position: Int) {
        onFootballTeamClickListener?.let { holder.bind(list[position], it) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FootballViewholder {
        return FootballViewholder(FootballItemUI().createView(AnkoContext.Companion.create(parent.context)))
    }

    override fun getItemCount(): Int = list.size

    class FootballViewholder(itemView: View?): RecyclerView.ViewHolder(itemView){

        val tvName: TextView = itemView?.findViewById(R.id.tvFootballName) as TextView

        val imgLogo: ImageView = itemView?.findViewById(R.id.imgFootballItemLogo) as ImageView

        val lnItem: LinearLayout = itemView?.findViewById(R.id.lnFootballItem) as LinearLayout

        fun bind(item: Item, onFootballTeamClickListener: OnFootballTeamClickListener){
            tvName.text = item.name
            Glide.with(itemView.context).load(item.logo).into(imgLogo)

            lnItem.setOnClickListener {
                onFootballTeamClickListener.onFootballTeamClicked(item)
            }
        }
    }

    interface OnFootballTeamClickListener{
        fun onFootballTeamClicked(item: Item)
    }
}