package com.nbs.footballteam.ui

import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.widget.LinearLayout
import com.nbs.footballteam.MainActivity
import com.nbs.footballteam.R
import com.nbs.footballteam.adapter.FootballAdapter
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView

/**
 * Created by sidiqpermana on 3/19/18.
 */
class MainActivityUI(val footballAdapter: FootballAdapter): AnkoComponent<MainActivity> {
    override fun createView(ui: AnkoContext<MainActivity>) = with(ui) {
        verticalLayout {
            lparams(width = matchParent, height = matchParent){
                padding = dip(16)
                orientation = LinearLayout.VERTICAL
            }

            recyclerView {
                lparams(width = matchParent, height = matchParent)
                id = R.id.rv_football_team
                setHasFixedSize(true)
                addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                adapter = footballAdapter
            }
        }
    }
}