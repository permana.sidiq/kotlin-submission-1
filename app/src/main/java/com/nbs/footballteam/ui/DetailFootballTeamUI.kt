package com.nbs.footballteam.ui

import android.view.Gravity
import android.widget.LinearLayout
import com.nbs.footballteam.DetailFootballTeamActivity
import com.nbs.footballteam.R
import org.jetbrains.anko.*

/**
 * Created by sidiqpermana on 3/20/18.
 */
class DetailFootballTeamUI: AnkoComponent<DetailFootballTeamActivity>{
    override fun createView(ui: AnkoContext<DetailFootballTeamActivity>) = with(ui) {
        linearLayout {
            lparams(width = matchParent, height = matchParent){
                orientation = LinearLayout.VERTICAL
                padding = dip(16)
            }

            imageView(R.drawable.default_image){
                id = R.id.imgDetailLogo
            }.lparams(width = dip(100), height = dip(100)){
                gravity = Gravity.CENTER
                bottomMargin = dip(16)
            }.adjustViewBounds = true

            textView("Description"){
                id = R.id.tvDescription
            }.lparams(width = matchParent, height = wrapContent)
        }
    }

}