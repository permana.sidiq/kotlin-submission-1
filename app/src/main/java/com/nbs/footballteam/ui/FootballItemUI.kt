package com.nbs.footballteam.ui

import android.content.Context
import android.view.Gravity
import android.widget.LinearLayout
import com.nbs.footballteam.R
import org.jetbrains.anko.*

/**
 * Created by sidiqpermana on 3/19/18.
 */
class FootballItemUI: AnkoComponent<Context>{
    override fun createView(ui: AnkoContext<Context>) = with(ui) {
        linearLayout {
            id = R.id.lnFootballItem
            lparams(width = matchParent, height =  wrapContent){
                padding = dip(16)
                orientation = LinearLayout.HORIZONTAL
            }

            imageView(R.drawable.default_image){
                id = R.id.imgFootballItemLogo
            }.lparams(width = dip(50),
                    height = dip(50)){
                marginEnd = dip(10)
                gravity = Gravity.CENTER_VERTICAL
            }

            textView("FootballName"){
                id = R.id.tvFootballName
                gravity = Gravity.CENTER_VERTICAL
            }.lparams(width = matchParent, height = wrapContent){
                gravity = Gravity.CENTER_VERTICAL
            }
        }
    }

}