package com.nbs.footballteam

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.nbs.footballteam.model.Item
import com.nbs.footballteam.ui.DetailFootballTeamUI
import org.jetbrains.anko.setContentView

class DetailFootballTeamActivity : AppCompatActivity() {

    companion object {
        val keyItem: String = "KEY_ITEM"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DetailFootballTeamUI().setContentView(this)

        val item = intent.getParcelableExtra<Item>(keyItem)

        val tvDescription = findViewById<TextView>(R.id.tvDescription)

        val imgDetailLogo = findViewById<ImageView>(R.id.imgDetailLogo)

        apply {
            supportActionBar?.title = item.name
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }

        tvDescription.text = item.description
        Glide.with(this).load(item.logo).into(imgDetailLogo)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home){
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
